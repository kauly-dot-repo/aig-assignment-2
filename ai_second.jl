### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ abe3c292-cef2-11eb-1d53-ad16b647f780
begin
	using Images
	using ImageIO
	using Flux
	using Flux.Data: DataLoader
	using Flux: onehotbatch, onecold, logitcrossentropy
    using Flux: @epochs
	using Plots
	
end

# ╔═╡ a6fe3cb0-cf8d-11eb-1e3f-e95c7655a883
using Printf

# ╔═╡ f8f859e0-cf9e-11eb-23fa-07ec4e8207e4
md"# Image Preprocessing"

# ╔═╡ fac58bf2-cef2-11eb-1e70-d38115051d1c
begin
	#Assumes current directory contains both image-containing folders.
	base_path = pwd() # present working direcctory
	#Finds all names of files in given path
	file_normal = readdir(base_path*"\\archive (1)\\chest_xray\\test\\NORMAL")
	file_pneumonia = readdir(base_path*"\\archive (1)\\chest_xray\\test\\PNEUMONIA")

	#Joins the paths with the file names
	path_x_test = base_path*"\\archive (1)\\chest_xray\\test\\NORMAL\\".*file_normal
	path_y_test = base_path*"\\archive (1)\\chest_xray\\test\\PNEUMONIA\\".*file_pneumonia

	x_normal_test = path_x_test
	y_pneumonia_test = path_y_test
	#Finds all file names in given path
	file_normal_train = readdir(base_path*"\\archive (1)\\chest_xray\\train\\NORMAL")
	file_pneumonia_train = readdir(base_path*"\\archive (1)\\chest_xray\\train\\PNEUMONIA")

	#Joins the paths with the file names
	path_x_normal_train = base_path*"\\archive (1)\\chest_xray\\train\\NORMAL\\".*file_normal_train
	path_y_pneumonia_train = base_path*"\\archive (1)\\chest_xray\\train\\PNEUMONIA\\".*file_pneumonia_train

	x_train = path_x_normal_train
	y_train  = path_y_pneumonia_train
	x_test_final = [x_normal_test; y_pneumonia_test]
	x_train_final = [path_x_normal_train; path_y_pneumonia_train]
end

# ╔═╡ c27a12c0-d20d-11eb-00ad-11c545a0074d
md"## Extracting Labels"

# ╔═╡ 2e113f0e-cf69-11eb-0c3f-7d36ff1444c6
pneumonia_labels_train = ["pneumonia"
	for file_pneumonia_train in file_pneumonia_train]

# ╔═╡ 426fb2d0-cf72-11eb-3594-b57e18b8b3e6
x_train[5]

# ╔═╡ 4c6cd2f0-cf6c-11eb-301d-97e7a2c385e5
normal_labels_train = ["normal" for file_normal_train in file_normal_train]

# ╔═╡ 5f453200-cf6c-11eb-09a8-77afa3bc3e87
pneumonia_labels_test = ["pneumonia" for x_normal_test in x_normal_test]

# ╔═╡ 5a494160-cf6c-11eb-1392-bfb16b22b670
normal_labels_test = ["normal" for path_y_test in path_y_test]

# ╔═╡ 6ccb9c30-cef3-11eb-0fb7-1b3d978968e6
# Loads the image, turns it to grayscale, resizes it. Converts it to 3D - when combined with training data, it will be turned to 4D
function process_image(path)
    img = load(path)
    img = Gray.(img)
    img = imresize(img,(28,28))
    img = Flux.unsqueeze(Float64.(img), 3)	
    return img
end

# ╔═╡ ceda4440-cf56-11eb-1e4f-81534f1cbc00
Xtrain =  [process_image.(x_train_final) for x_train_final in  x_train_final]

# ╔═╡ ee384470-cf8f-11eb-3699-136df40034f9
Xtest =  [process_image.(x_test_final) for x_test_final in  x_test_final]

# ╔═╡ ce782710-cf56-11eb-0e8a-fd8ceb9119b0
Ytrain =[normal_labels_train; pneumonia_labels_train]

# ╔═╡ 41b8c110-cf81-11eb-07b6-81de17e0ca73
Ytest =[normal_labels_test; pneumonia_labels_test]

# ╔═╡ 83cdaa80-cf71-11eb-12d4-674d410bf890
YStrain = [Flux.onehotbatch(Ytrain,["normal","pneumonia"]) for label in Ytrain]

# ╔═╡ 9b39e892-cf7c-11eb-1f39-07cdf37cb447
YStest = [Flux.onehotbatch(Ytest,["normal","pneumonia"]) for label in Ytest]

# ╔═╡ b58871f0-cf7a-11eb-2ad3-43f3b01e14d7
train_loader = DataLoader((Xtrain, YStrain), shuffle=true ,batchsize=128)

# ╔═╡ c6810e20-cf72-11eb-0665-b3907a123bff
training_batch = (Flux.batch(Xtrain),Flux.batch(YStrain))


# ╔═╡ d9cb63b0-cf84-11eb-2b85-899d079c3729
testing_batch = (Flux.batch(Xtest),Flux.batch(YStest))

# ╔═╡ fb0a82f2-d20d-11eb-29b8-0b1232fd91c6
md"## LeNet 5 Architecture definition"

# ╔═╡ 59ac6be0-cfbe-11eb-270d-d17cf34570a8
function LeNet5(; imgsize=(28,28,1), nclasses=2) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
          
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            x -> reshape(x, :, size(x, 4)),
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses),
		softmax
          )
end

# ╔═╡ bed9b610-d042-11eb-3896-01825d9101cc
Iterators.repeated(training_batch,128)

# ╔═╡ 3a3c5662-d20e-11eb-2f7c-2bb51b7d4faa
md"# Training"

# ╔═╡ 98151c60-cf91-11eb-2c3e-217cdbaedd61
@epochs 10 Flux.train!(loss,ps,Iterators.repeated(training_batch,10),opt,cb = Flux.throttle(update_loss! ,1))

# ╔═╡ c7deff60-cf91-11eb-0e22-51d2c88462b1


# ╔═╡ f548d250-cf91-11eb-37ca-d138e318523f
 #Plotting the training loss
	plot(1:length(train_loss),train_loss,xlabel="Secnods of Training")
	
	


# ╔═╡ 4b3e4de0-d044-11eb-3a0f-e95ea6ff5509
plot(1:length(test_loss),test_loss,xlabel="Seconds of Training")

# ╔═╡ 6e814920-cfbc-11eb-1084-853256df2c1c
plot!(1:length(test_loss),test_loss,label="test")

# ╔═╡ 5fdd8a40-d044-11eb-03d9-95530128083d
x_train[50]

# ╔═╡ 31b775c0-d050-11eb-2009-d130a447f478
model

# ╔═╡ eebd4a50-d03c-11eb-1e3b-55b419686409
Ytrain[50],findmax(model(Flux.batch(process_image.(x_train[50])),2)) .- (0,1)

# ╔═╡ 09c83c80-d054-11eb-0e1c-1f7c6fc99c25
model

# ╔═╡ 11438500-d054-11eb-1854-23618fe6e0ea
findmax(model(Flux.batch(Xtrain[50])))[1]

# ╔═╡ 1007e5d0-d056-11eb-3d86-03feff638445
Flux.batch(Xtrain[5])

# ╔═╡ 16a50352-d056-11eb-27f4-95b18efc34f1
Xtrain[5]

# ╔═╡ fe8fb2fe-d055-11eb-39ce-5d850b943db2
findmax(model(Flux.batch(Xtrain[5])))[1]

# ╔═╡ cc38956e-d055-11eb-17b2-8dbfd0bc18e9
Flux.batch(Xtrain[50])

# ╔═╡ c2fb51a0-d055-11eb-2468-3d70a1c41638
Xtrain[50]

# ╔═╡ d975ac12-d054-11eb-1bd5-d3bb4459f94b
findmax(model(Flux.batch(process_image.(x_train[50]))))[1]


# ╔═╡ 94e8f490-d053-11eb-3970-d19190a891aa
model(Flux.batch(process_image.(x_train[50])))

# ╔═╡ 1f78acae-d052-11eb-182b-55fea4edbf77
training_batch[5][5]

# ╔═╡ b66b2b90-d050-11eb-0255-e1d52ce21013
process_image.(x_train[50])

# ╔═╡ 7ed92660-cf91-11eb-08ec-6bd42d2bf27d
#Prediction
prediction(i) = findmax(model(Flux.batch(process_image.(x_train[i])))[2]-1

# ╔═╡ 260d3660-d20d-11eb-00fa-071cf32305ce


# ╔═╡ eacdfae0-cf90-11eb-24d0-e944a3ea71bc
begin
training_loss = Float64[]
testing_loss = Float64[]
accuracy = Float64[]
	
	function update_loss!()
		push!(training_loss,loss(training_batch...))
		push!(testing_loss,loss(testing_batch...))
        push!(accuracy,accuracy(testing_batch...,model))
		@printf("train loss = %.2f,test loss=%.2f,Test Accuracy %.2f\n",loss(training_batch...),loss(testing_batch...),accuracy(testing_batch...,model))

	end
end

# ╔═╡ ced507c0-cf90-11eb-0720-473977fb6208
training_loss = Float64[]

# ╔═╡ e6687de2-cf90-11eb-2d0b-c50a20b09311
testing_loss = Float64[]

# ╔═╡ 91253c10-cf72-11eb-1903-93dfb7dcfc91
begin
	model = LeNet5()
	loss(x,y) = Flux.logitcrossentropy(model(x),y)
	accuracy(x,y,f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
	ps = Flux.params(model)
    opt = ADAM(3e-4)
	#@time Flux.train!(loss,ps,[training_batch],opt)
end

# ╔═╡ Cell order:
# ╠═abe3c292-cef2-11eb-1d53-ad16b647f780
# ╟─f8f859e0-cf9e-11eb-23fa-07ec4e8207e4
# ╠═fac58bf2-cef2-11eb-1e70-d38115051d1c
# ╠═c27a12c0-d20d-11eb-00ad-11c545a0074d
# ╠═2e113f0e-cf69-11eb-0c3f-7d36ff1444c6
# ╠═426fb2d0-cf72-11eb-3594-b57e18b8b3e6
# ╠═4c6cd2f0-cf6c-11eb-301d-97e7a2c385e5
# ╠═5f453200-cf6c-11eb-09a8-77afa3bc3e87
# ╠═5a494160-cf6c-11eb-1392-bfb16b22b670
# ╠═6ccb9c30-cef3-11eb-0fb7-1b3d978968e6
# ╠═ceda4440-cf56-11eb-1e4f-81534f1cbc00
# ╠═ee384470-cf8f-11eb-3699-136df40034f9
# ╠═ce782710-cf56-11eb-0e8a-fd8ceb9119b0
# ╠═41b8c110-cf81-11eb-07b6-81de17e0ca73
# ╠═83cdaa80-cf71-11eb-12d4-674d410bf890
# ╠═9b39e892-cf7c-11eb-1f39-07cdf37cb447
# ╠═b58871f0-cf7a-11eb-2ad3-43f3b01e14d7
# ╠═c6810e20-cf72-11eb-0665-b3907a123bff
# ╠═d9cb63b0-cf84-11eb-2b85-899d079c3729
# ╠═fb0a82f2-d20d-11eb-29b8-0b1232fd91c6
# ╠═59ac6be0-cfbe-11eb-270d-d17cf34570a8
# ╠═bed9b610-d042-11eb-3896-01825d9101cc
# ╠═91253c10-cf72-11eb-1903-93dfb7dcfc91
# ╠═a6fe3cb0-cf8d-11eb-1e3f-e95c7655a883
# ╠═ced507c0-cf90-11eb-0720-473977fb6208
# ╠═e6687de2-cf90-11eb-2d0b-c50a20b09311
# ╠═eacdfae0-cf90-11eb-24d0-e944a3ea71bc
# ╠═3a3c5662-d20e-11eb-2f7c-2bb51b7d4faa
# ╠═98151c60-cf91-11eb-2c3e-217cdbaedd61
# ╟─c7deff60-cf91-11eb-0e22-51d2c88462b1
# ╠═f548d250-cf91-11eb-37ca-d138e318523f
# ╠═4b3e4de0-d044-11eb-3a0f-e95ea6ff5509
# ╠═6e814920-cfbc-11eb-1084-853256df2c1c
# ╠═5fdd8a40-d044-11eb-03d9-95530128083d
# ╠═31b775c0-d050-11eb-2009-d130a447f478
# ╠═eebd4a50-d03c-11eb-1e3b-55b419686409
# ╠═09c83c80-d054-11eb-0e1c-1f7c6fc99c25
# ╠═11438500-d054-11eb-1854-23618fe6e0ea
# ╠═1007e5d0-d056-11eb-3d86-03feff638445
# ╠═16a50352-d056-11eb-27f4-95b18efc34f1
# ╠═fe8fb2fe-d055-11eb-39ce-5d850b943db2
# ╠═cc38956e-d055-11eb-17b2-8dbfd0bc18e9
# ╠═c2fb51a0-d055-11eb-2468-3d70a1c41638
# ╠═d975ac12-d054-11eb-1bd5-d3bb4459f94b
# ╠═94e8f490-d053-11eb-3970-d19190a891aa
# ╠═1f78acae-d052-11eb-182b-55fea4edbf77
# ╠═b66b2b90-d050-11eb-0255-e1d52ce21013
# ╠═7ed92660-cf91-11eb-08ec-6bd42d2bf27d
# ╠═260d3660-d20d-11eb-00fa-071cf32305ce
